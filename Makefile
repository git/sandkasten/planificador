format:
	find src/ \( -name '*.cpp' -o -name '*.hpp' \) -exec clang-format -i {} +

.PHONY: format
