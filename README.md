planificador
===========

A sandbox execution environment for untrusted code. It acts as a front-end in front of Docker+Bubblewrap.

Tasks are submitted using a ZeroMQ message queue, allowing quick scaling of the system.

Protocol
--------

*planificador* receives messages from a ZeroMQ queue in binary format in big-endian.

Executor bound
--------------

The first byte of the message is the message type. The following types are supported:

* `0x00`: `SUBMIT` - Submit a new task to the system.
* `0x02`: `CANCEL` - Cancel a task.

The following bytes are the payload of the message. The format of the payload depends on the message type.

### SUBMIT

- 32 bytes: Task ID
- 4 bytes: Image field length
- 4 bytes: Code length
- Image field length bytes: Image field
- Code length bytes: Code

### CANCEL

- 32 bytes: Task ID

Client bound
------------

The first byte of the message is the message type. The following types are supported:

* `0x01`: `APPEND_OUT` - Append text to the task's stdout.
* `0x02`: `APPEND_ERR` - Append text to the task's stderr.
* `0x03`: `EXITED` - The task has exited.

## APPEND_OUT / APPEND_ERR

- 32 bytes: Task ID
- 4 bytes: Text length
- Text length bytes: Text

## EXITED

- 32 bytes: Task ID
- 4 bytes: Exit code
