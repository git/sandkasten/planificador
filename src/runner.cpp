#include "runner.hpp"

#include <algorithm>
#include <array>
#include <cerrno>
#include <fcntl.h>
#include <poll.h>
#include <spawn.h>
#include <sys/timerfd.h>
#include <sys/wait.h>
#include <system_error>
#include <unistd.h>

// Define a helper to throw a system error if a syscall fails
static auto ensure = [](int res) -> void {
    if (res == -1) {
        throw std::system_error{errno, std::generic_category()};
    }
};

template <class> inline constexpr bool always_false_v = false;

namespace sk {
execution_strategy::execution_strategy(const std::vector<std::string> &patterns) {
    for (const auto &pattern : patterns) {
        if (pattern == "{}") {
            this->patterns.emplace_back(nullptr);
        } else {
            this->patterns.emplace_back(pattern);
        }
    }
}

execution_strategy::execution_strategy(std::vector<pattern> patterns) : patterns(std::move(patterns)) {}

std::unique_ptr<execution_strategy> execution_strategy::create(const runner_strategy_config &config) {
    return std::visit(
        [](auto &&strategy) -> std::unique_ptr<execution_strategy> {
            using T = std::decay_t<decltype(strategy)>;
            if constexpr (std::is_same_v<T, sk::bubblewrap_config>) {
                return std::make_unique<bubblewrap_execution_strategy>(strategy.args);
            } else if constexpr (std::is_same_v<T, sk::docker_config>) {
                return std::make_unique<docker_execution_strategy>(strategy.args, strategy.image);
            } else {
                static_assert(always_false_v<T>, "non-exhaustive visitor!");
            }
        },
        config);
}

void execution_strategy::concat_patterns(std::vector<const char *> &args, const program &program) const {
    for (const auto &pattern : patterns) {
        std::visit(
            [&args, &program](const auto &arg) {
                if constexpr (std::is_same_v<std::string, std::decay_t<decltype(arg)>>) {
                    args.push_back(arg.c_str());
                } else {
                    args.push_back(program.code.c_str());
                }
            },
            pattern);
    }
}

bubblewrap_execution_strategy::bubblewrap_execution_strategy(const std::vector<std::string> &patterns) : execution_strategy{patterns} {}

bubblewrap_execution_strategy::bubblewrap_execution_strategy(std::vector<pattern> patterns) : execution_strategy{std::move(patterns)} {}

std::vector<const char *> bubblewrap_execution_strategy::start(const program &program) {
    std::vector<const char *> args{"bwrap", "--ro-bind", "/usr", "/usr", "--dir", "/tmp", "--dir", "/var", "--proc", "/proc", "--dev", "/dev", "--symlink", "usr/lib", "/lib", "--symlink", "usr/lib64", "/lib64", "--symlink", "usr/bin", "/bin", "--symlink", "usr/sbin", "/sbin", "--unshare-all", "--die-with-parent"};
    concat_patterns(args, program);
    return args;
}

void bubblewrap_execution_strategy::stop(const active_job &job) { ensure(kill(job.pid, SIGINT)); }

docker_execution_strategy::docker_execution_strategy(const std::vector<std::string> &patterns, std::string image) : execution_strategy{patterns}, image{std::move(image)} {}

docker_execution_strategy::docker_execution_strategy(std::vector<pattern> patterns, std::string image) : execution_strategy{std::move(patterns)}, image{std::move(image)} {}

std::vector<const char *> docker_execution_strategy::start(const program &program) {
    std::vector<const char *> args{"docker", "run", "--rm", "-i", "--name", program.name.c_str(), "--pull=never", "--cap-drop=ALL", "--network=none", "--memory=64m", "--memory-swap=64m", "--pids-limit=128", image.c_str()};
    concat_patterns(args, program);
    return args;
}

void docker_execution_strategy::stop(const active_job &job) {
    const char *const kill_args[] = {"docker", "kill", job.job_id.c_str(), nullptr};
    pid_t kill_pid;
    ensure(posix_spawnp(&kill_pid, kill_args[0], nullptr, nullptr, const_cast<char *const *>(kill_args), nullptr));
}

runner::runner(const runner_strategy_config &config, unsigned int timeout) : backend{execution_strategy::create(config)}, timeout{static_cast<int>(timeout)} {}

runner::runner(const runner &other) : backend{&*other.backend}, timeout{other.timeout} {}

runner::runner(runner &&other) noexcept : backend{std::move(other.backend)}, timeout{other.timeout} {}

runner &runner::operator=(const runner &other) {
    if (this == &other) {
        return *this;
    }
    std::unique_ptr<execution_strategy> new_backend(&*other.backend);
    backend = std::move(new_backend);
    timeout = other.timeout;
    return *this;
}

runner &runner::operator=(runner &&other) noexcept {
    if (this == &other) {
        return *this;
    }
    backend = std::move(other.backend);
    timeout = other.timeout;
    return *this;
}

run_result runner::run_blocking(const program &program) {
    // Open file descriptors ahead of time
    int in_pipe[2];
    int out_pipe[2];
    int err_pipe[2];
    if (pipe2(in_pipe, O_CLOEXEC) == -1 || pipe2(out_pipe, O_CLOEXEC) == -1 || pipe2(err_pipe, O_CLOEXEC) == -1) {
        throw std::system_error{errno, std::generic_category()};
    }

    // Create a timer that will be polled when the program runs too long
    int timerfd = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC);
    if (timerfd == -1) {
        throw std::system_error{errno, std::generic_category()};
    }
    itimerspec timer{};
    timer.it_value.tv_sec = timeout;
    if (timerfd_settime(timerfd, 0, &timer, nullptr) == -1) {
        throw std::system_error{errno, std::generic_category()};
    }

    // Avoid blocking on stdin, and being interrupted if the pipe is closed prematurely
    int flags = fcntl(in_pipe[1], F_GETFL, 0);
    fcntl(in_pipe[1], F_SETFL, flags | O_NONBLOCK);

    posix_spawn_file_actions_t actions;
    posix_spawn_file_actions_init(&actions);
    posix_spawn_file_actions_addclose(&actions, timerfd);
    posix_spawn_file_actions_adddup2(&actions, in_pipe[0], STDIN_FILENO);
    posix_spawn_file_actions_adddup2(&actions, out_pipe[1], STDOUT_FILENO);
    posix_spawn_file_actions_adddup2(&actions, err_pipe[1], STDERR_FILENO);
    std::vector<const char *> args = backend->start(program);
    args.push_back(nullptr);
    pid_t pid;
    bool killed = false;
    int exit_code;
    if (posix_spawnp(&pid, args[0], &actions, nullptr, const_cast<char *const *>(args.data()), nullptr) != 0) {
        throw std::system_error{errno, std::generic_category()};
    }

    close(in_pipe[0]);
    close(out_pipe[1]);
    close(err_pipe[1]);

    // Register the job as active
    {
        std::lock_guard<std::mutex> guard(active_jobs_mutex);
        active_jobs.push_back(active_job{program.name, pid});
    }

    size_t len = program.code.size();
    size_t window = 0;
    const char *data = program.code.data();

    std::array<char, 1024> buffer{};
    std::string out;
    std::string err;
    std::array<pollfd, 4> plist = {pollfd{in_pipe[1], POLLOUT | POLLHUP, 0}, pollfd{out_pipe[0], POLLIN, 0}, pollfd{err_pipe[0], POLLIN, 0}, pollfd{timerfd, POLLIN, 0}};
    pollfd *pfds = plist.data();
    nfds_t nfds = plist.size();

    int res;
    while ((res = poll(pfds, nfds, -1)) > 0 || (res == -1 && errno == EINTR)) {
        if (res == -1) {
            // Interrupted by a signal, retry
            continue;
        }

        nfds_t polled_fds = 0;
        if (nfds == plist.size()) {
            // Poll stdin when we still have data to write
            if (pfds[0].revents & (POLLHUP | POLLERR)) {
                // Closing input prematurely
                close(in_pipe[1]);
                --nfds;
                ++pfds;
                ++polled_fds;
            } else if (pfds[0].revents & POLLOUT) {
                if (window >= len) {
                    // End input
                    close(in_pipe[1]);
                    --nfds;
                    ++pfds;
                } else {
                    // Write the current data window
                    size_t l = std::min(len - window, buffer.size());
                    write(pfds[0].fd, data + window, l);
                    window += l;
                }
                ++polled_fds;
            }
        }

        // Poll stdout, stderr and the timer
        for (nfds_t i = nfds - 3; i < nfds; ++i) {
            if (pfds[i].revents & POLLIN) {
                ssize_t bytes_read = read(pfds[i].fd, buffer.data(), buffer.size());
                if (bytes_read == -1) {
                    throw std::system_error{errno, std::generic_category()};
                }
                if (pfds[i].fd == out_pipe[0]) {
                    out.append(buffer.data(), bytes_read);
                } else if (pfds[i].fd == timerfd) {
                    std::lock_guard<std::mutex> guard(active_jobs_mutex);
                    auto it = std::find_if(active_jobs.begin(), active_jobs.end(), [pid](const active_job &job) { return job.pid == pid; });
                    if (it != active_jobs.end()) {
                        exit(*it);
                        active_jobs.erase(it);
                    }
                    killed = true;
                } else {
                    err.append(buffer.data(), bytes_read);
                }
                ++polled_fds;
            }
        }

        // If nothing was polled, we're done
        if (polled_fds == 0) {
            break;
        }
    }

    waitpid(pid, &exit_code, 0);
    close(out_pipe[0]);
    close(err_pipe[0]);
    close(timerfd);

    // Remove the job from the active list
    {
        std::lock_guard<std::mutex> guard(active_jobs_mutex);
        auto it = std::find_if(active_jobs.begin(), active_jobs.end(), [pid](const active_job &job) { return job.pid == pid; });
        if (it != active_jobs.end()) {
            active_jobs.erase(it);
        }
    }

    posix_spawn_file_actions_destroy(&actions);
    return run_result{out, err, killed ? 124 : exit_code};
}

bool runner::kill_active(const std::string &jobId) {
    std::lock_guard<std::mutex> guard(active_jobs_mutex);
    auto it = std::find_if(active_jobs.begin(), active_jobs.end(), [&jobId](const active_job &job) { return job.job_id == jobId; });
    if (it != active_jobs.end()) {
        exit(*it);
        active_jobs.erase(it);
        return true;
    }
    return false;
}

void runner::exit_active_jobs() {
    std::lock_guard<std::mutex> guard(active_jobs_mutex);
    for (const auto &job : active_jobs) {
        exit(job);
    }
    active_jobs.clear();
}

void runner::exit(const active_job &job) { backend->stop(job); }

runner_list::runner_list(sk::runner_backend preferred_backend, const runner_config &config) {
    for (const auto &strategy : config.strategies) {
        runners.emplace(std::string{strategy.first}, runner{strategy.second, config.timeout});
    }
}

runner *runner_list::find_runner_for(const program &program) {
    auto it = runners.find(program.image);
    if (it != runners.end()) {
        return &it->second;
    }
    return nullptr;
}

bool runner_list::kill_active(const std::string &jobId) {
    return std::any_of(runners.begin(), runners.end(), [&jobId](auto &runner) { return runner.second.kill_active(jobId); });
}

void runner_list::exit_active_jobs() {
    for (auto &[name, runner] : runners) {
        runner.exit_active_jobs();
    }
}
}
