#pragma once

#include <cstddef>
#include <cstdint>
#include <string_view>
#include <tuple>
#include <zmq.hpp>

namespace sk {

static constexpr uint32_t JOB_ID_LEN = 32;

uint32_t read_uint32(const std::byte *buffer);
void write_uint32(std::byte *buffer, uint32_t value);
void write_string(std::byte *buffer, std::string_view text);

std::tuple<zmq::message_t, std::byte *> prepare_headers(size_t data_len, int type, std::string_view jobId);
}
