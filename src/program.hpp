#pragma once

#include <string>
#include <vector>

namespace sk {
struct program {
    std::string name;
    std::string code;
    std::string image;
};
}
