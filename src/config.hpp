#pragma once

#include <filesystem>
#include <string>
#include <unordered_map>
#include <variant>
#include <vector>

namespace sk {
struct queue_config {
    std::string pull_addr;
    std::string push_addr;
    unsigned int nb_threads;
};

struct bubblewrap_config {
    std::vector<std::string> args;
};

struct docker_config : bubblewrap_config {
    std::string image;
};

using runner_strategy_config = std::variant<bubblewrap_config, docker_config>;

struct runner_config {
    unsigned int timeout;
    std::unordered_map<std::string, runner_strategy_config> strategies;
};

struct config {
    queue_config queue;
    runner_config runner;

    static config read_or_default(const std::filesystem::path &path, bool expect_present = false);
};
}
