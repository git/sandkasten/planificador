#include "network.hpp"

#include <cstring>

namespace sk {
uint32_t read_uint32(const std::byte *buffer) { return static_cast<uint32_t>(buffer[3]) | static_cast<uint32_t>(buffer[2]) << 8 | static_cast<uint32_t>(buffer[1]) << 16 | static_cast<uint32_t>(buffer[0]) << 24; }
void write_uint32(std::byte *buffer, uint32_t value) {
    buffer[0] = static_cast<std::byte>(value >> 24);
    buffer[1] = static_cast<std::byte>(value >> 16);
    buffer[2] = static_cast<std::byte>(value >> 8);
    buffer[3] = static_cast<std::byte>(value);
}

void write_string(std::byte *buffer, std::string_view text) {
    auto size = static_cast<uint32_t>(text.size());
    write_uint32(buffer, size);
    memcpy(buffer, text.data(), size);
}

std::tuple<zmq::message_t, std::byte *> prepare_headers(size_t data_len, int type, std::string_view jobId) {
    zmq::message_t reply(1 + JOB_ID_LEN + data_len);
    auto *reply_bytes = static_cast<std::byte *>(reply.data());
    *reply_bytes = static_cast<std::byte>(type);
    memcpy(reply_bytes + 1, jobId.data(), JOB_ID_LEN);
    return {std::move(reply), reply_bytes + 1 + JOB_ID_LEN};
}
}
