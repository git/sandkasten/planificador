#include "config.hpp"

#include <iostream>
#include <toml++/toml.h>

static sk::config read(const toml::table &config) {
    std::unordered_map<std::string, sk::runner_strategy_config> strategies;
    auto *table = config["runners"].as_table();
    if (table != nullptr) {
        for (auto [name, backend_config] : *table) {
            auto *backend_table = backend_config.as_table();
            if (backend_table == nullptr) {
                std::cerr << "Invalid runner config for " << name << ": expected table\n";
                continue;
            }
            auto *backend_type = backend_table->get_as<std::string>("type");
            if (backend_type == nullptr) {
                std::cerr << "Invalid runner config for " << name << ": missing type field\n";
                continue;
            }
            auto *args = backend_table->get_as<toml::array>("args");
            if (args == nullptr) {
                std::cerr << "Invalid runner config for " << name << ": missing args field\n";
                continue;
            }
            std::vector<std::string> args_vector;
            for (const auto &arg : *args) {
                auto *arg_string = arg.as_string();
                if (arg_string == nullptr) {
                    std::cerr << "Invalid runner config for " << name << ": args must be strings\n";
                    continue;
                }
                args_vector.emplace_back(*arg_string);
            }
            if (*backend_type == "docker") {
                auto *image = backend_table->get_as<std::string>("image");
                if (image == nullptr) {
                    std::cerr << "Invalid runner config for " << name << ": missing image field\n";
                    continue;
                }
                strategies.emplace(name, sk::docker_config{{std::move(args_vector)}, std::string{*image}});
            } else if (*backend_type == "bubblewrap") {
                strategies.emplace(name, sk::bubblewrap_config{std::move(args_vector)});
            } else {
                std::cerr << "Invalid runner config for " << name << ": unknown type " << *backend_type << "\n";
            }
        }
    }
    return sk::config{
        sk::queue_config{
            config["queue"]["pull"].value_or("tcp://localhost:5557"),
            config["queue"]["push"].value_or("tcp://localhost:5558"),
            config["queue"]["threads"].value_or(1u),
        },
        sk::runner_config{
            config["runner"]["timeout"].value_or(2u),
            std::move(strategies),
        },
    };
}

sk::config sk::config::read_or_default(const std::filesystem::path &path, bool expect_present) {
    std::ifstream t(path);
    if (!t) {
        if (errno == ENOENT && !expect_present) {
            std::cout << "Using default config\n";
        } else {
            std::cerr << "Failed to open config file " << path << ": " << strerror(errno) << "\n";
        }
        return read(toml::table{});
    }
    std::stringstream buffer;
    buffer << t.rdbuf();
    auto config = toml::parse(buffer.str(), path);
    return read(config);
}
