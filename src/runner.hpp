#pragma once

#include "config.hpp"
#include "program.hpp"
#include <memory>
#include <mutex>
#include <string>
#include <variant>
#include <vector>

namespace sk {
struct [[nodiscard]] run_result {
    std::string out;
    std::string err;
    int exit_code;
};

struct [[nodiscard]] active_job {
    std::string job_id;
    pid_t pid;
};

enum class runner_backend { BubbleWrap, Docker };
using pattern = std::variant<std::string, void *>;

class execution_strategy {
    std::vector<pattern> patterns;

  public:
    explicit execution_strategy(const std::vector<std::string> &patterns);
    explicit execution_strategy(std::vector<pattern> patterns);
    virtual std::vector<const char *> start(const program &program) = 0;
    virtual void stop(const active_job &job) = 0;
    virtual ~execution_strategy() = default;
    static std::unique_ptr<execution_strategy> create(const runner_strategy_config &config);

  protected:
    void concat_patterns(std::vector<const char *> &args, const program &program) const;
};

class bubblewrap_execution_strategy : public execution_strategy {
  public:
    explicit bubblewrap_execution_strategy(const std::vector<std::string> &patterns);
    explicit bubblewrap_execution_strategy(std::vector<pattern> patterns);
    std::vector<const char *> start(const program &program) override;
    void stop(const active_job &job) override;
};

class docker_execution_strategy : public execution_strategy {
  private:
    std::string image;

  public:
    docker_execution_strategy(const std::vector<std::string> &patterns, std::string image);
    docker_execution_strategy(std::vector<pattern> patterns, std::string image);
    std::vector<const char *> start(const program &program) override;
    void stop(const active_job &job) override;
};

class runner {
    std::vector<active_job> active_jobs;
    std::mutex active_jobs_mutex;
    std::unique_ptr<execution_strategy> backend;
    int timeout;

  public:
    explicit runner(const runner_strategy_config &config, unsigned int timeout = 2u);
    runner(const runner &other);
    runner(runner &&other) noexcept;
    runner &operator=(const runner &other);
    runner &operator=(runner &&other) noexcept;
    run_result run_blocking(const program &program);
    bool kill_active(const std::string &jobId);
    void exit_active_jobs();
    void exit(const active_job &job);
};

class runner_list {
    std::unordered_map<std::string, runner> runners;

  public:
    runner_list(sk::runner_backend preferred_backend, const runner_config &config);
    runner *find_runner_for(const program &program);
    bool kill_active(const std::string &jobId);
    void exit_active_jobs();
};
}
