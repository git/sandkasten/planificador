#include "network.hpp"
#include "program.hpp"
#include "runner.hpp"
#include <csignal>
#include <filesystem>
#include <iostream>
#include <spawn.h>
#include <sys/wait.h>
#include <toml++/toml.h>
#include <unistd.h>

#include "config.hpp"
#include "zmq_addon.hpp"

static constexpr uint32_t MIN_SUBMIT_MESSAGE_LEN = sk::JOB_ID_LEN + sizeof(uint32_t) * 2;
static constexpr uint32_t MIN_CANCEL_MESSAGE_LEN = sk::JOB_ID_LEN + sizeof(uint32_t);

static constexpr int SUBMIT_EXECUTOR_BOUND = 0;
static constexpr int CANCEL_EXECUTOR_BOUND = 1;
static constexpr int STDOUT_CLIENT_BOUND = 1;
static constexpr int STDERR_CLIENT_BOUND = 2;
static constexpr int EXIT_CLIENT_BOUND = 3;

sk::runner_backend detect_backend() {
    const char *const argv[] = {"docker", "stats", "--no-stream", nullptr};
    pid_t pid;
    if (posix_spawnp(&pid, argv[0], nullptr, nullptr, const_cast<char *const *>(argv), nullptr) != 0) {
        return sk::runner_backend::BubbleWrap;
    }
    int status = 0;
    waitpid(pid, &status, 0);
    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
        std::cerr << "Using Docker" << std::endl;
        return sk::runner_backend::Docker;
    }
    return sk::runner_backend::BubbleWrap;
}

sk::runner_list *global_runners = nullptr;

int main(int argc, char **argv) {
    int opt;
    std::optional<sk::runner_backend> selected_backend;
    std::filesystem::path config_path("config.toml");
    bool may_use_default_config = true;
    while ((opt = getopt(argc, argv, "bc:d")) != -1) {
        switch (opt) {
        case 'b':
            selected_backend = sk::runner_backend::BubbleWrap;
            break;
        case 'c':
            config_path = optarg;
            may_use_default_config = false;
            break;
        case 'd':
            selected_backend = sk::runner_backend::Docker;
            break;
        default:
            std::cerr << "Usage: %s [-bc] [script]\n";
            return 1;
        }
    }

    sk::config config = sk::config::read_or_default(config_path, !may_use_default_config);
    sk::runner_list runners(selected_backend.has_value() ? selected_backend.value() : detect_backend(), config.runner);
    global_runners = &runners;
    struct sigaction action {};
    action.sa_handler = [](int) {
        if (global_runners) {
            global_runners->exit_active_jobs();
        }
    };
    sigaction(SIGINT, &action, nullptr);
    sigaction(SIGTERM, &action, nullptr);

    if (optind < argc) {
        std::ifstream t(argv[optind]);
        if (!t) {
            std::cerr << "Unable to open file " << argv[optind] << std::endl;
            return 1;
        }
        std::stringstream buffer;
        buffer << t.rdbuf();
        std::string code = buffer.str();
        sk::program program{"sample-code", code, "moshell"};
        sk::runner *runner = runners.find_runner_for(program);
        if (runner == nullptr) {
            std::cerr << "No runner found for " << program.image << std::endl;
            return 1;
        }
        sk::run_result result = runner->run_blocking(program);
        std::cout << "exited with: " << result.exit_code << "\n";
        std::cout << "out: " << result.out << "\n";
        std::cout << "err: " << result.err << "\n";
        return 0;
    }

    zmq::context_t context(static_cast<int>(config.queue.nb_threads));
    zmq::socket_t receiver(context, zmq::socket_type::pull);
    receiver.connect(config.queue.pull_addr);
    zmq::socket_t sender(context, zmq::socket_type::push);
    sender.connect(config.queue.push_addr);

    auto send = [&sender](int type, const std::string &jobId, const std::string &text) {
#ifndef NDEBUG
        std::cout << "Result: `" << text << "`\n";
#endif
        auto [reply, reply_bytes] = sk::prepare_headers(sizeof(uint32_t) + text.size(), type, jobId);
        sk::write_string(reply_bytes, text);
        sender.send(reply, zmq::send_flags::none);
    };

    while (true) {
        zmq::message_t request;
        zmq::recv_result_t _ = receiver.recv(request);
        const auto *message = static_cast<const char *>(request.data()) + 1;
        auto *message_bytes = static_cast<const std::byte *>(request.data()) + 1;
        int message_type = static_cast<int>(*static_cast<const unsigned char *>(request.data()));
        switch (message_type) {
        case SUBMIT_EXECUTOR_BOUND: {
            if (request.size() < MIN_SUBMIT_MESSAGE_LEN) {
                std::cerr << "Invalid request\n";
                continue;
            }
            std::string jobId(message, sk::JOB_ID_LEN);
            uint32_t imageLen = sk::read_uint32(message_bytes + sk::JOB_ID_LEN);
            uint32_t codeLen = sk::read_uint32(message_bytes + sk::JOB_ID_LEN + sizeof(uint32_t));

            if (request.size() < MIN_SUBMIT_MESSAGE_LEN + imageLen + codeLen) {
                std::cerr << "Request is too short\n";
                continue;
            }
            std::string imageString(message + MIN_SUBMIT_MESSAGE_LEN, imageLen);
            std::string requestString(message + MIN_SUBMIT_MESSAGE_LEN + imageLen, codeLen);

#ifndef NDEBUG
            std::cout << "Executing " << codeLen << " bytes code.\n";
#endif
            sk::program program{std::move(jobId), std::move(requestString), std::move(imageString)};
            sk::runner *runner = runners.find_runner_for(program);
            if (runner == nullptr) {
                send(STDERR_CLIENT_BOUND, program.name, "No runner found for " + program.image);
                sk::prepare_headers(sizeof(uint32_t), EXIT_CLIENT_BOUND, program.name);
                auto [reply, reply_bytes] = sk::prepare_headers(sizeof(uint32_t), EXIT_CLIENT_BOUND, program.name);
                sk::write_uint32(reply_bytes, -1);
                sender.send(reply, zmq::send_flags::none);
                continue;
            }
            sk::run_result result = runner->run_blocking(program);

            if (!result.out.empty()) {
                send(STDOUT_CLIENT_BOUND, program.name, result.out);
            }
            if (!result.err.empty()) {
                send(STDERR_CLIENT_BOUND, program.name, result.err);
            }
            auto [reply, reply_bytes] = sk::prepare_headers(sizeof(uint32_t), EXIT_CLIENT_BOUND, program.name);
            sk::write_uint32(reply_bytes, result.exit_code);
            sender.send(reply, zmq::send_flags::none);
            break;
        }
        case CANCEL_EXECUTOR_BOUND: {
            if (request.size() < MIN_CANCEL_MESSAGE_LEN) {
                std::cerr << "Invalid request\n";
                continue;
            }
            std::string jobId(message, sk::JOB_ID_LEN);
            runners.kill_active(jobId);
            break;
        }
        default:
            std::cerr << "Invalid " << std::hex << message_type << " message type\n";
            break;
        }
    }
    return 0;
}
