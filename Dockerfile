FROM alpine:3.18 as builder

WORKDIR /app

RUN apk add --no-cache build-base git zeromq-dev cmake \
    && apk add --no-cache cppzmq --repository=https://dl-cdn.alpinelinux.org/alpine/edge/testing

COPY . .
RUN cmake -B build -S . && cmake --build build --parallel $(nproc)

FROM alpine:3.18 as runner

RUN apk add --no-cache libc++ zeromq
COPY --from=builder /app/build/planificador /usr/local/bin/planificador

ENTRYPOINT ["/usr/local/bin/planificador"]
